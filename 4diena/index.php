<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Spalvynas</title>
  <link href="https://fonts.googleapis.com/css?family=Alegreya+SC:400,700|Trocchi" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">

</head>
<body>
  <section class="nulinis">
    <header>
      <div id="site-navigation">
        <img src="img/logo-header.png" alt="spalvynas logo">
        <ul>
          <li><a href="#apiemus">Apie mus</a></li>
          <li><a href="#paslaugos">Paslaugos</a></li>
          <li><a href="#kodelmes">Kodėl mes?</a></li>
          <li><a href="#kaipmesdirbame">Kaip mes dirbame?</a></li>
          <li><a href="#kontaktai">Kontaktai</a></li>
        </ul>
        <div class="sandwich">
          <div class="bar1"></div>
          <div class="bar2"></div>
          <div class="bar3"></div>
        </div>
      </div>
    </header>
    <section>
      <span><img src="img/phone.png" alt="">+370 685 33412</span>
      <h1>Dažų gamyba ir prekyba</h1>
      <h2>Automobiliams, laivams, metalinems konstrukcijoms ir t.t.</h2>
      <img class="bounce" src="img/arrow.png" alt="dažų plunksna">
    </section>
  </section>

  <section class="pirmas" id="apiemus">
    <h1><img src="img/logo-vienas.png" alt="spalvynas logo">TAI -</h1>
    <section class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <h2>darbštumas</h2>
          <p>Vien per pastaruosius<br>metus turėjome 500<br>klientų</p>
        </div>
        <div class="col-md-4">
          <h2>PATIRTIS</h2>
          <p>Dirbame daugiau<br>kaip 22<br>metus</p>
        </div>

        <div class="col-md-4">
          <h2>PATIKIMUMAS</h2>
          <p>50 pastovių klientų su<br>kuriais dirbame<br>daugiau kaip 20 metų</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <h2>TĘSTINUMAS</h2>
          <p>Esame padarę dažus<br>perdažimui daugiau nei<br>20000 daužtų mašinų</p>
        </div>
        <div class="col-md-4">
          <h2>įvairovė</h2>
          <p>Reguliariai sunaudojame 100kg<br>dažų spalvų<br> gamybai</p>
        </div>
        <div class="col-md-4">
          <h2>efektyvumas</h2>
          <p>Greito užsakymo<br>galimybė. Įprastų<br> užsakymų vykdymas<br>iki 1 dienos</p>
        </div>

      </div>
    </section>
  </section>
  <section class="antras" id="paslaugos">
    <section class="container-fluid">
      <h1>Kokias paslaugas siūlome?</h1>
      <div class="row">
        <div class="col-md-4">
          <img src="img/idea.png" alt="idėjos dažymui">
          <p>Dažų parinkimas ir<br> gamyba</p>
        </div>
        <div class="col-md-4">
          <img src="img/painter.png" alt="dažyti">
          <p>Prekiaujame<br>medžiagomis<br> susijusiomis<br> su dažymu</p>
        </div>
        <div class="col-md-4">
          <img src="img/spray.png" alt="purškiami balionėliai">
          <p>Gaminame<br>
purškiamus dažus<br> balionėliuose</p>
        </div>
      </div>
    </section>

  </section>
  <section class="trys">
    <section>
      <h1>Klientai kuriais didžiuojamės</h1>
      <div class="owl-carousel">

        <img src="img/salna logo.jpeg" alt="šalna logo">


        <img src="img/kad-nebutu-salta.png" alt="Kad nebūtų šalta">


        <img src="img/vaidmina.png" alt="Vaidmina">


        <img src="img/retro-garazas.png" alt="retro-garazas">


        <img src="img/lukera.jpg" alt="lukera">


        <img src="img/uab-kamile.jpg" alt="uab kamilė">

      </div>
      <h1>Turite klausimų?</h1>
      <div class="trys-footer">
        <img src="img/zenkliukas-debeer.jpg" alt="debeer">
        <button>Susisiekite</button>
        <img src="img/zenkliukas-debeer.jpg" alt="debeer">
      </div>
      <div class="clearfix"></div>
    </section>
  </section>
  <section class="keturi" id="kodelmes">
    <h1>9 priežastys kodėl jau 22 metus<br>klientai renkasi mus</h1>
    <section class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <h2>1</h2>
          <p>Tiksliai parenkame<br>dažus</p>
        </div>
        <div class="col-md-4">
          <h2>2</h2>
          <p>Greitas užsakymo<br>atlikimas</p>
        </div>

        <div class="col-md-4">
          <h2>3</h2>
          <p>Konsultuojame</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <h2>4</h2>
          <p>Greitas reagavimas į <br>užklausas</p>
        </div>
        <div class="col-md-4">
          <h2>5</h2>
          <p>Patrauklios kainos</p>
        </div>
        <div class="col-md-4">
          <h2>6</h2>
          <p>Platus pasirinkimas</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <h2>7</h2>
          <p>Mokėjimo atidėjimo <br>galimybė</p>
        </div>
        <div class="col-md-4">
          <h2>8</h2>
          <p>Vidutinė darbuotojų<br>patirtis daugiau kaip<br>10 metų</p>
        </div>
        <div class="col-md-4">
          <h2>9</h2>
          <p>Galime parduoti ar <br>pagaminti įvairius<br> mažus kiekius</p>
        </div>
      </div>
    </section>
  </section>
  <section class="penki" id="kaipmesdirbame">
    <section>
      <h1>Kaip mes dirbame</h1>
      <div class="pirmas-penki">
        <img src="img/1.png" alt="Vienas">
        <p>Jūs pristatote<br>detalės pavyzdį</p>
      </div>
      <div class="antras-penki">
        <img src="img/2.png" alt="Du">
        <p>Jeigu nėra dažų kodo <br>susirandame pagal<br>paletę</p>
      </div>
      <div class="trecias-penki">
        <img src="img/3.png" alt="Trys">
        <p>Gaminame dažus</p>
      </div>
      <div class="ketvirtas-penki">
        <img src="img/4.png" alt="Keturi">
        <p>Pagaminę tikriname</p>
      </div>
      <div class="penktas-penki">
        <img src="img/5.png" alt="Penktas">
        <p>Pasiimate įvykdytą<br>užsakymą</p>
      </div>
      <img src="img/gyvatele.png" alt="paint snake">
      <button>Palikti užklausą</button>
      <div class="tarpas"></div>
    </section>
  </section>
  <section class="sesi" id="kontaktai">
    <div id="map"></div>
  </section>
  <footer class="septyni">
    <section>
      <h1>Liko klausimų?</h1>
      <h1>Skambinkite</h1><h1>+370 685 33412</h1>
      <h1>arba...</h1>
      <div class="marked">
        <div style="height:20px"></div>
        <button>Rašykite</button>
        <p>Rekvizitai:<br>
          Adresas: Tilžės g. 45,<br>
          Klaipėda<br>
          Facebook: spalvynas<br>
          El.p: labas@spalvynas.lt
        </p>
      </div>
      <div style="height:50px"></div>
    </section>
  </footer>
  <script src="js/jquery-3.2.1.min.js" charset="utf-8"></script>
  <script type="text/javascript">
        function initMap() {
                var myLatlng = {lat: 55.7045106, lng: 21.153831200000013};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 17,
                  center: myLatlng,
                  scrollwheel: false,
                  zoomControl: true,
                  scaleControl: false,
                  disableDoubleClickZoom: false,
                });
                var contentString = '<div id="content">'+
                     '<div id="siteNotice">'+
                     '</div>'+
                     '<h5 id="firstHeading" class="firstHeading">UAB "Spalvynas"</h5>'+
                     '<div id="bodyContent">'+
                     '<p>Adresas: Tilžės g. 45, Klaipėda</p>'+
                     '<p>Tel: +370 685 33412</p>'+
                     '<p>El.p: labas@spalvynas.lt</p>'+
                     '</div>'+
                     '</div>';

                 var infowindow = new google.maps.InfoWindow({
                   content: contentString
                 });
                var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                });
                map.addListener('mouseover', function() {
                    infowindow.open(map, marker);

                });
                map.addListener('mouseout', function(){
                    infowindow.close();
                });
                google.maps.event.addListenerOnce(map, 'idle', function() {
                    google.maps.event.trigger(map, 'resize');
                });
             }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6kBfHqystCeVax-xCcAwgb3a7RhPcBEY&callback=initMap">

    </script>
    <script src="js/owl.carousel.min.js" charset="utf-8"></script>
    <script src="js/navigation.js" charset="utf-8"></script>
</body>
</html>
