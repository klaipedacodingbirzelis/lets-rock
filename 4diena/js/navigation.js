$(function(){
	// mobile sandwich
	$('.sandwich').click(function(){
								if($('.sandwich>div').hasClass('change')){
									$('.bar1, .bar2,.bar3').removeClass('change');
									$('#site-navigation ul').toggle(1000);

								}
								else {
									$('.bar1, .bar2,.bar3').addClass('change');
									$('#site-navigation ul').toggle(1000);

								}
	});
	$(document).click(function (e) {
	    e.stopPropagation();
	    var container = $("#site-navigation");
	    var windowWidth = window.innerWidth;
	    //check if the clicked area is dropDown or not
	    if (container.has(e.target).length === 0 && windowWidth <= 680) {
	        $('.bar1, .bar2,.bar3').removeClass('change');
	        $('#site-navigation ul').hide(1000);

	    }
	});
	// owl carousel
	$(".owl-carousel").owlCarousel({
								 items:2,
								 nav: false,
								 loop:true,
								 autoplay:true,
								 autoplayTimeout:4000,
								 autoplayHoverPause:true,
								 slidespeed: 200,
								 paginationSpeed: 400,
								 singleItem:true,
								 responsiveClass:true,
							   responsive:{
							                480:{
							                    items:1
							                },
							                800:{
							                    items:1
							                },
							                1200: {
							                	  items:2
							                },
							            }
	});

	function setHeight() {
        windowHeight = $(window).innerHeight();
        $('.container-fluid.starting').css('min-height', windowHeight);
    };
    setHeight();

    $(window).resize(function () {
        setHeight();
    });

    // Smooth scroll
    // Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]').not('[href="#0"]').click(function (event) {
        // On-page links
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1200, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) {
                        // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });

    $('.bounce').click(function () {
        event.preventDefault();
        var n = $(window).height()+44;
        $('html, body').animate({ scrollTop: n }, 400);
    });
		// fadein balioneliai
		var foto = document.querySelectorAll('.antras .col-md-4 img');
		console.log(foto);
		var distanceFromTop =document.querySelector('img[src="img/idea.png"]').parentNode.parentNode.offsetTop;
		var seen = false;
		window.addEventListener('scroll', function(){
			console.log(distanceFromTop);
	    var view1 = window.scrollY + window.outerHeight - 300;
	    if(view1 > distanceFromTop) {
					for (var i = 0; i < foto.length; i++) {
						foto[i].style.transform = "scale(1)";
					}
	        seen = true;
	    };
		});
});
