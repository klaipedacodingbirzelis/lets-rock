<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grybukas
 */

?>

<article id="post-<?php the_ID();
?>" <?php post_class(); ?>>
	<div class="entry-content">
		<section class="nulinis">
			<div class="mid">
		    <div class="vidurys">
		      <h1 class="pastry">
						<?php
							the_content();

							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'grybas' ),
								'after'  => '</div>',
							) );
						?>

		      </h1>
		      <img src="<?php echo(get_template_directory_uri()); ?>/img/dashed.png" alt="dashed">
		      <p>
						<?php echo get_post_meta( get_the_ID(), 'data', true );?>
					</p>
		      <button type="button" name="button">Our Menu</button>
		  </div>


		</section>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'grybas' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
