<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package grybukas
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">

			<div class="bar2">
				<div class="inner">
			 <?php
			 wp_nav_menu( array(
				 'theme_location' => 'footerKaire',
				 'menu_id'        => 'FooterLeft',
			 ) );
			 ?>
			  <img class="gustoso2" src="<?php bloginfo('template_url'); ?>/img/gustoso2.png" alt="gustoso">
			 <?php
			 wp_nav_menu( array(
				 'theme_location' => 'footerDesine',
				 'menu_id'        => 'FooterRight',
			 ) );
			 ?>
			 	</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
