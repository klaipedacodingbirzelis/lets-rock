<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package grybukas
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header bar">
		<nav id="site-navigation" class="main-navigation">
			<a href=""><img class="gustoso" src="<?php bloginfo('template_url'); ?>/img/gustoso.png" alt="gustoso"></a>
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'grybas' ); ?></button>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
			<a href=""><img class="logo" src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="icons"></a>
		 </nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	<div id="content" class="site-content">
