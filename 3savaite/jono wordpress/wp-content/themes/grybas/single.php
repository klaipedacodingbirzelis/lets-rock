<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package grybukas
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<!-- single post kaip atrodis -->
			<div class="juosta3"></div>
			<?php // Display blog posts on any page
			$id = get_the_ID();
			$args = array(
			  'p'         => $id,
			  'post_type' => 'any'
			);
			$temp = $wp_query; $wp_query= null;
			$wp_query = new WP_Query($args);
			?>
			<?
			while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
			<section class="slider-vidinis">
				<?php the_post_thumbnail('vidinis-slider') ?>
				<div class="inner">

					<h1><?php the_title(); ?></h1>
					<h2><?php echo get_post_meta( $id, 'event-data', true );?></h2>
				</div>
			</section>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
			<section class="blocks2">
				<div>
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'content' );


					endwhile; // End of the loop.
					?>
					<h2>Paskutiniai įrašai</h2>
					<ul>
					<?php
						$args = array( 'numberposts' => '5' );
						$recent_posts = wp_get_recent_posts( $args );
						foreach( $recent_posts as $recent ){
							echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"] . ' ' . date('d/m/Y', strtotime($recent['post_date'])).'</a> </li> ';
						}
						wp_reset_query();
					?>
					</ul>
				</div>
			</section>
			Sasyskos page
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
