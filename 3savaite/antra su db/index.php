<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Klaipeda coding</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	
</head>

<body>
	<?php 

	$vaisiusGet = $_GET['vaisius'];
	$vaisiusPost = $_POST['vaisius'];
	 ?>
	<nav class="navbar navbar-inverse navbar-fixed-top">
	     <div class="container">
	       <div class="navbar-header">
	         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	           <span class="sr-only">Toggle navigation</span>
	           <span class="icon-bar"></span>
	           <span class="icon-bar"></span>
	           <span class="icon-bar"></span>
	         </button>
	         <a class="navbar-brand" href="#">Project name</a>
	       </div>
	       <div id="navbar" class="collapse navbar-collapse">
	         <ul class="nav navbar-nav">
	           <li class="active"><a href="#">Home</a></li>
	           <li><a href="#about">About</a></li>
	           <li><a href="#contact">Contact</a></li>
	         </ul>
	       </div><!--/.nav-collapse -->
	     </div>
	   </nav>
		
	   <div class="container" style="margin-top: 200px">
			<div class="row">
				<div class="col-md-12">
					<!-- <form class="" action="post.php" method="get">
			          <div>
			            <label for="vaisius">Vaisiaus pavadinimas
			              <input type="text" name="vaisius" value="" id="vaisius">
			            </label>
			          </div>
			          <div>
			            <button type="submit" name="button" value="Send">Išsiųsti</button>
			          </div>
	        		</form> -->
	        		<form class="" action="post.php" method="get">
			          <div>
			            <label for="vaisius">Vaisiaus pavadinimas
			              <input type="text" name="vaisius" value="" id="vaisius">
			            </label>
			          </div>
			          <div>
			            <button type="submit" name="button" value="Send">Išsiųsti</button>
			          </div>
	        		</form>
				</div>
			</div>
	    

	   </div><!-- /.container -->
	   
			<h1><?php echo $vaisiusGet; ?></h1>
		
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		 <!-- Latest compiled and minified JavaScript -->
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>