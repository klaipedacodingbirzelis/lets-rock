// Classical OOP(oriented object programming)

// Objekto konstruktorius, rasomas didziaja raide
// function Person(vardas, pavarde){
//   this.name = vardas;
//   this.surname = pavarde;
//
//   this.getName = function(){
//     return this.name;
//   }
// };
//
// var Nerijus = new Person("Nerijus", "Sin");
// Nerijus.amzius = 38;
// var Justas = new Person("Justas", "Sarpalius");
// console.log(Nerijus);
// console.log(Nerijus.getName());
// console.log(Justas);

// Prototypes - protypical pattern:

// var person = {
//   species: "human",
//   saySpecis: function(){
//     console.log(this.species);
//   }
// };
// var musician = Object.create(person);
// console.log(musician);

// function Futbolininkas(vardas,pavarde,greitis,ugis,amzius){
//   this.name = vardas;
//   this.surname = pavarde;
//   this.speed = greitis;
//   this.height = ugis;
//   this.age = amzius;
//   this.getName = function () {
//     return this.name;
//   };
//   this.setName = function (abrakadabra) {
//     this.name = abrakadabra;
//   };
//   this.getSurname = function() {
//     return this.surname;
//   }
//   this.getSpeed = function() {
//     return this.speed;
//   }
//   this.getHeight = function () {
//     return this.height;
//   }
//   this.getAge = function () {
//     return this.age;
//   }
// }
// var Grytute = new Futbolininkas("Gritute", "Ispasakos", 104, 174, 14);
// Grytute.setName("Grytute");
// console.log(Grytute.getName());
// var Zbignewas = new Futbolininkas("Zbignewas", "Lenkowskis", 101, 150, 44);
// var Ipolitas = new Futbolininkas("Ipolitas","Linkmasis",100, 155, 25);
//
// if(Grytute.getSpeed()> Zbignewas.getSpeed() && Grytute.getSpeed() > Ipolitas.getSpeed()){
//   document.querySelector("p").innerHTML = Grytute.getName()+ " belenkaip greit bega" + " ir jos greitis yra:" + Grytute.getSpeed();
  // console.log(Grytute.getName()+ " belenkaip greit bega" + " ir jos greitis yra:" + Grytute.getSpeed());
// } else if (Grytute.getSpeed()< Zbignewas.getSpeed() && Zbignewas.getSpeed() > Ipolitas.getSpeed()){
  // console.log(Zbignewas.getName()+ " belenkaip greit bega" + " ir jo greitis yra:" + Zbignewas.getSpeed());
// } else {
  // console.log(Ipolitas.getName()+ " belenkaip greit bega" + " ir jo greitis yra:" + Ipolitas.getSpeed());
// }
//
// var masyvasSuVardais = [Grytute.getName(),Zbignewas.getName(), Ipolitas.getName()];
// console.log(masyvasSuVardais);
// var issortintas = masyvasSuVardais.sort();
// console.log(issortintas);
//
// for (var i = 0; i < issortintas.length; i++) {
//   document.querySelector("ul").innerHTML += '<li>' +issortintas[i]+ '</li>';
// }

function Masina(pav,greitis,nuvkelias) {
    this.name = pav;
    this.speed= greitis;
    this.road = nuvkelias;

    this.getName = function () {
      return this.name;
    }

    this.getSpeed = function () {
      return this.speed;
    }
    this.getRoad = function () {
      return this.road;
    }
    this.getAvg = function () {
      return this.road/this.speed;
    }
}
var i = 0;
var masinuArray = [];
var didziausiasSort = [];

document.querySelector('button').addEventListener("click", function(){
  var inputai = document.querySelectorAll('input');
  var pavadinimas= inputai[0].value;
  var greitis= inputai[1].value;
  var nuvkelias= inputai[2].value;
  var susikurtuMasina = new Masina(pavadinimas,greitis,nuvkelias);

  document.querySelector('table').innerHTML += '<tr>' +'<td>' +(i++)+ '</td>' + '<td>' +susikurtuMasina.getName()+ '</td>' + '<td>' +susikurtuMasina.getSpeed()+ '</td>' +
  '<td>' +susikurtuMasina.getRoad()+ '</td>' + '<td>' +susikurtuMasina.getAvg()+ '</td>' + '</tr>';

  // masinuArray.push(susikurtuMasina.getName() susikurtuMasina.getAvg());
  // console.log(masinuArray);
  // for (var j = 0; j < masinuArray.length; j++) {
  //   for (var key in masinuArray[j]) {
  //     // if(key == speed){
  //     //   console.log('tinkamas key');
  //     // }
  //
  //     console.log(key + ":" + masinuArray[j][key]);
  //     // console.log(getAvg);
  //   }
  // }
  // var ats = didziausiasSort.sort();
  // console.log(ats);
});
