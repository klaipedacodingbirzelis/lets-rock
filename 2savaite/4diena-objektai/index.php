<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Objektai ir schema.org</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <!-- <h1>Greičiausias bėgikas</h1> -->
  <p></p>
  <ul>

  </ul>
  <label for="pav">Mašinos pavadinimas</label>

  <input type="text" name="pav" placeholder="Pavadinimas">
  <label for="pav">Mašinos greitis</label>
  <input type="text" name="speed" placeholder="Greitis">
  <label for="pav">Mašinos nuvažiuotas kelias</label>
  <input type="text" name="road" placeholder="Kelias">
  <button type="button" name="button">Pridėti mašiną</button>

  <table style="border:1px solid black">
    <thead>
      <th>Eilės Nr.</th>
      <th>Pavadinimas</th>
      <th>Greitis</th>
      <th>Įveiktas kelias</th>
      <th>Greičio vidurkis</th>
    </thead>

  </table>
  <h2>Greičiausias automobilis</h2>
  <p id="greiciausias"></p>
  <script src="custom.js" charset="utf-8"></script>
</body>
</html>
