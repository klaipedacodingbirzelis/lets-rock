// var a = 5;
//
// console.log("veikia!");
// console.log(a);
//
// var a = 10;
// var b = 5;
// console.log(b, a);
// var c = a-b;
// console.log(c);
// var result = c;

// Kintamųjų rūšys

// var one = 1;
// var two = "Žodis";
// var three = true;
// var four;
//
// var five=1,
//     Five=2;
//
// console.log(one);
// console.log(two);
// console.log(three);
// console.log(four);
//
// console.log(typeof(one));
// console.log(typeof(two));
// console.log(typeof(three));
// console.log(typeof(four));
//
// console.log(five, Five);

// Savybės ir metodai tekstinėms eilutėms

// var pirmas = "Pirmas";
// tekstines eilutes ilgis:
// console.log(pirmas.length);

// Didziosiomis:
// console.log(pirmas.toUpperCase());
// Mazosiomis:
// console.log(pirmas.toLowerCase());
// Isveda tekstines eilutes simboli pagal eile:
// console.log(pirmas.charAt(1));
// Isveda ieskomos raides ar zodzio pozicija eiluteje:
// console.log(pirmas.indexOf("ma"));

// Metodai su skaiciais

// var first = 12.14;

// Grazina sveikaji skaiciu:

// console.log(parseInt(first, 10));

// Grazina misruji skaiciu:
// console.log(parseFloat(first));

// Kai reikia skaiciu paversti stringu:
// console.log(first.toString());

// Nurodo kiek skaiciu po kablelio isvesti:
// console.log(first.toFixed(3));
// Nurodo kiek isvesti bendrai:
// console.log(first.toPrecision(3));

// Logines reiksmes:

// var first = 2,
//     second = "2";
//
// var relationship = (first != second);
// console.log(relationship);

// var yes = 2,
//     no;
//
// console.log(Boolean(yes));
// console.log(Boolean(no));


// Masyvai

// var studentai = ["Mindaugas","Giedrius","Justas","Jonas", "Mantas", "Egilė"];
// var papildomai = ["vienas", "du"];
// console.log(studentai);
// studentai[0] = "Nerijus";
// console.log(studentai[0]);
// console.log(studentai);
// studentai[7] = papildomai;
// console.log(studentai);
// console.log(studentai[7][0]);

// var luckyNumbers = "1,2,3,4,5,6,7,22,99,55";
// var luckArray = luckyNumbers.split(",");
// console.log(luckArray);


// var number = 2;
//
// console.log(number++);
// console.log(number);

// number = 2;
// snumber = 80;
// console.log(number);
// number = number+ snumber;
// number += snumber;
// console.log(number);

// Salyginiai sakiniai
// var result = 6;
//
// if (result >= 9) {
//   console.log("Mldc");
// } else if ((result >= 5) && (result <= 8)){
//   console.log("belenkoks mldc");
// } else {
//   console.log("Nu gal ir mldc");
// }

// Uzduotis klaseje

// var Maryte= 3;
// var Jonas = 4;
// var bendrai = Maryte + Jonas;
// console.log("Bendra obuoliu suma" + bendrai);
//
// bendrai += 4;
// console.log("Padidejo per" + bendrai);
// bendrai -= 8;
// console.log("Sumazejo per" + bendrai);
//
// if(Maryte>Jonas){
//   console.log("Maryte turi daugiau obuoliu");
// } else if (Jonas>Maryte) {
//   console.log("Jonas turi daugiau obuoliu");
// } else if (Jonas == Maryte) {
//   console.log("Atiduokim obuolius Nerijui");
// } else {
//   console.log("Nera obuoliu");
// }
//
// switch (bendrai) {
//   case 7:
//     console.log("Turime pirma case");
//     break;
//   default:
//     console.log("Visi kiti atvejai");
// }
//
// var vaisiai = ["Bananai", "Obuoliai", "Citrinos", "Abrikosai", "Mandarinai", "Apelsinai"];
//
// console.log(vaisiai[2], vaisiai[4]);
// for (var i = 0; i < vaisiai.length; i++) {
//   console.log(vaisiai[i]);
// }

// Objektai

// var pc = {};
//
// pc.gamintojas = "lenova";
// pc.os = ["windows", "linux"];
// pc.likutis = 3;
// pc.parametrai = {
//   "procesorius" : "i7",
//   "ram" : 8,
//   "atmintis": 240,
//   "lieciamas ekranas": true
// };
//
//
// console.log(pc);
// console.log(pc.gamintojas);
// console.log(pc.os[1]);
// console.log(pc.parametrai.procesorius);
// console.log(pc.parametrai["lieciamas ekranas"]);

// For in
var weather = {
  "Pirmadienis" : 2,
  "Antradienis" : -3,
  "Treciadienis" : 4
}
console.log("Oru prognoze:");
for (var key in weather) {
  console.log(key+ ": " + weather[key]);
}
document.querySelector('a').innerHTML = "Linkas pas Neriju i svecius";

document.getElementById("button").addEventListener("click",  function(){
  document.getElementById("section").style.display='none';
});
document.getElementById("button").addEventListener("mouseover",  function(){
  // this.style.fontSize = "60px";
  // alert("Nepaspausi:)");
});
// paimti elementa su tuo id.
// prideti eventa = mouseover
// pasitikrinti ar veikia funkcija
// paimti elementa, kuriam norim pakeisti stiliu(jei reikia pasitikrinam)
// surasti kaip pakeisti stiliu elementui per javascipta
// pakeisti stiliu i koki reikia
// document.getElementById("veikia").addEventListener("mouseover", function(){
//   document.getElementById("veikia").style.border = "3px solid blue";
// })
// document.getElementById("veikia").addEventListener("mouseout", function(){
//   document.getElementById("veikia").style.border = "none";
// })
// paimti elementa input
// document.getElementById("kazkoksinput");
// paimti mygtuka
// document.getElementById("mygtukas");
// prideti paspaudimo eventa
// document.getElementById("mygtukas").addEventListener("click", function(){
  // paimti parasyta is inputo teksta
  // kaip paimti inputo reiksme per javascripta
  // prideti zodi i diva
  // kaip prideti reiksme i diva per javascripta
// });

var number = 5;
// number += 9;
number = number +9;
console.log(number);
